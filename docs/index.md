# 기계 학습 개념 소개 <sup>[1](#footnote_1)</sup>

> <font size="3">기계 학습이 무엇이고 어떻게 작동하는지 알아본다.</font>

## 목차

1. [기계 학습이란?](./introduction.md#sec_01)
1. [기계 학습의 종류](./introduction.md#sec_02)
1. [기계 학습 워크플로우](./introduction.md#sec_03)
1. [기계 학습 응용 프로그램](./introduction.md#sec_04)
1. [기계 학습 과제](./introduction.md#sec_05)
1. [기계 학습 리소스](./introduction.md#sec_06)


<a name="footnote_1">1</a>: [ML Tutorial 1 — Introduction to Machine Learning Concepts](https://medium.com/ai-in-plain-english/ml-tutorial-1-introduction-to-machine-learning-concepts-0f440f768a57?sk=f3fdd4bfeaa4466a989eb4a48388c25d)을 편역한 것입니다.
